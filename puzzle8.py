import random
import tkinter as tk
import tkinter.messagebox
from PIL import Image, ImageTk


pictures = 	[		{'id': '0', 'nombre': 'num.jpg', 		'msj':'El juego de puzzle-8 es un rompecabezas deslizante que consiste en un marco de fichas cuadradas numeradas en orden aleatorio a la que le falta una ficha.' }, 
					{'id': '1', 'nombre': 'turing.png', 	'msj':'En 1950 Turing inició las bases del Aprendizaje Autómatico con el artículo Computing machinery and intelligence'}, 
					{'id': '2', 'nombre': 'hinton.png', 	'msj':'Geoffrey Hinton presentó el concepto de Deep Learning en 2006 y es el co-creador de las máquinas de Boltzmann'},
					{'id': '3', 'nombre': 'anosuper.png',	'msj':'Aprendizaje no supervisado no necesita que un humano proporcione una guia y se basa en buscar similitudes con los datos y crear agrupamientos'}, 
					{'id': '4', 'nombre': 'arefuerzo.png', 	'msj':'Aprendizaje reforzado mejora continuamente su modelo al aprovechar la retroalimentacion y me jora su desempeño en función de su aprendizaje y experiencia pasada'}, 
					{'id': '5', 'nombre': 'asuper.png', 	'msj':'Aprendizaje supervisado requiere de un científico de datos que actue como guía y requiere un conjunto de datos ya etiquetado a una salida'},
					{'id': '6', 'nombre': 'machine.png', 	'msj':'Aprendizaje autómatico es un subconjunto de la IA que se centra en desarrollar en sistemas que aprende o mejoran el rendimiento en función de los datos que consumen'}]



MAX_BOARD_SIZE = 500
tab=[9,9,9,9,9,9,9,9,9]


class Application(tk.Frame):
	def resoluble(self):
		random.shuffle(self.board)
		j=0
		k=0
		invert=0
		for i in range(0,9):
			if i%3==0 and i!= 0:
				j += 1
				k = 0
			tab[k*3+j] = self.board[i]['pos_o'][0]+self.board[i]['pos_o'][1]*3
			#print(self.board[i]['pos_o'])
			#print(self.board[i]['pos_o'][0]+self.board[i]['pos_o'][1]*3)
			#print(tab)
			k += 1
		for m in range(0,9):
			count=0
			for n in range ((m+1),9):
				if tab[m] > tab[n] and tab[m]!= 8:
					count += 1
				
			#print(count)
			invert += count
		#print("Invertidas = ",invert)
		if invert%2==0:
			return 1
		else:
			return 0
	def __init__(self, image, board_grid=4):
		tk.Frame.__init__(self)
		self.grid()
		self.board_grid = board_grid if board_grid > 2 else 3
		self.load_image(image)
		self.steps = 0
		self.create_widgets()
		self.create_events()
		self.create_board()
		self.show()
	def load_image(self, image):
		image = Image.open(image)
		board_size = min(image.size)
		if image.size[0] != image.size[1]:
			image = image.crop((0, 0, board_size, board_size))
		if board_size > MAX_BOARD_SIZE:
			board_size = MAX_BOARD_SIZE
			image = image.resize((board_size, board_size), Image.ANTIALIAS)
		self.image = image
		self.board_size = board_size
		self.piece_size = self.board_size / self.board_grid
	def create_widgets(self):
		args = dict(width=self.board_size, height=self.board_size)
		self.canvas = tk.Canvas(self, **args)
		self.canvas.grid()

	def create_events(self):
		self.canvas.bind_all('<KeyPress-Up>', self.slide)
		self.canvas.bind_all('<KeyPress-Down>', self.slide)
		self.canvas.bind_all('<KeyPress-Left>', self.slide)
		self.canvas.bind_all('<KeyPress-Right>', self.slide)
		self.canvas.bind_all('<KeyPress-h>', self.slide)
		self.canvas.bind_all('<KeyPress-j>', self.slide)
		self.canvas.bind_all('<KeyPress-k>', self.slide)
		self.canvas.bind_all('<KeyPress-l>', self.slide)
		self.canvas.bind_all('<KeyPress-H>', self.help)

	def help(self, event):
		if getattr(self, '_img_help_id', None) is None:
			self._img_help = ImageTk.PhotoImage(self.image)
			self._img_help_id = self.canvas.create_image(0, 0,
					image=self._img_help, anchor=tk.NW)
		else:
			state = self.canvas.itemcget(self._img_help_id, 'state')
			state = 'hidden' if state == '' else ''
			self.canvas.itemconfigure(self._img_help_id, state=state)

	def slide(self, event):
		pieces = self.get_pieces_around()
		if event.keysym in ('Up', 'k') and pieces['bottom']:
			self._slide(pieces['bottom'], pieces['center'], 
						(0, -self.piece_size))
		if event.keysym in ('Down', 'j') and pieces['top']:
			self._slide(pieces['top'], pieces['center'],
						(0, self.piece_size))
		if event.keysym in ('Left', 'h') and pieces['right']:
			self._slide(pieces['right'], pieces['center'],
						(-self.piece_size, 0))
		if event.keysym in ('Right', 'l') and pieces['left']:
			self._slide(pieces['left'], pieces['center'],
						(self.piece_size, 0))
		self.check_status()

	def _slide(self, from_, to, coord):
		self.canvas.move(from_['id'], *coord)
		to['pos_a'], from_['pos_a'] = from_['pos_a'], to['pos_a']
		self.steps += 1

	def get_pieces_around(self):
		pieces = {'center': None,
				  'right' : None,
				  'left'  : None,
				  'top'   : None,
				  'bottom': None}
		for piece in self.board:
			if not piece['visible']:
				pieces['center'] = piece
				break
		x0, y0 = pieces['center']['pos_a']
		for piece in self.board:
			x1, y1 = piece['pos_a']
			if y0 == y1 and x1 == x0 + 1:
				pieces['right'] = piece
			if y0 == y1 and x1 == x0 - 1:
				pieces['left'] = piece
			if x0 == x1 and y1 == y0 - 1:
				pieces['top'] = piece
			if x0 == x1 and y1 == y0 + 1:
				pieces['bottom'] = piece
		return pieces

	def create_board(self):
		self.board = []
		for x in range(self.board_grid):
			for y in range(self.board_grid):
				x0 = x * self.piece_size
				y0 = y * self.piece_size
				x1 = x0 + self.piece_size
				y1 = y0 + self.piece_size
				image = ImageTk.PhotoImage(
						self.image.crop((x0, y0, x1, y1)))
				piece = {'id'     : None,
						 'image'  : image,
						 'pos_o'  : (x, y),
						 'pos_a'  : None,
						 'visible': True}
				self.board.append(piece)
		self.board[-1]['visible'] = False

	def check_status(self):
		for piece in self.board:
			if piece['pos_a'] != piece['pos_o']:
				return
		title = 'RESUELTO!!!'
		message = 'Lo resolviste en %d movidas!' % self.steps 
		tkinter.messagebox.showinfo(title, message)
		title2 = '¿Sabías qué???'
		message2 = pictures[n]['msj']
		tkinter.messagebox.showinfo(title2, message2)
		

			

	def show(self):
		random.shuffle(self.board)
		while 1:
			if self.resoluble()== 1:
				#print("Se puede resolver")
				break	
		index = 0
		for x in range(self.board_grid):
			for y in range(self.board_grid):
				self.board[index]['pos_a'] = (x, y)
				if self.board[index]['visible']:
					x1 = x * self.piece_size
					y1 = y * self.piece_size
					image = self.board[index]['image']
					id = self.canvas.create_image(
							x1, y1, image=image, anchor=tk.NW)
					self.board[index]['id'] = id
				index += 1




repeat=1
tutorial=1
if __name__ == '__main__':
    
	from optparse import OptionParser
	
	while repeat== 1 :
		n=random.randrange(0,10000)%6+1
		parser = OptionParser(description="Sliding puzzle")
		parser.add_option('-g', '--board-grid', type=int, default=3, help="(the minimum value is 3)")
		parser.add_option('-i', '--image', type=str, default=pictures[n]['nombre'], help="path to image")
		args, _ = parser.parse_args()
		

		if args.board_grid < 3:
			args.board_grid = 3
			print("Warning: using 3 for board-grid")

		app = Application(args.image, args.board_grid)
		app.master.title('Rompecabezas')
		app.mainloop()
		resp=tkinter.messagebox.askquestion('Juego Finalizado', 'Quieres jugar de nuevo?')
		if resp == 'yes':
			repeat=1
		else:
			repeat=0	

		
